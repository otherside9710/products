# Test for Falabella Retail

![N|Solid](https://programaenlinea.net/wp-content/uploads/2019/07/java.spring.png)

## Microservice
Spring Boot App

### Table of Contents
* [Stack](#stack)
* [Architecture Stack](#architecture)
* [Summary](#summary)
* [Requirements](#requirements)
* [Configuration](#configuration)
* [Project contents](#project-contents)
* [Run](#run)
* [Endpoints](#ENDPOINTS)

### Stack
Java Spring Boot + JPA + Postgres DB

### Architecture
Clean Architecture + Hexagonal Architecture + Clean Code + TDD + DDD

### Summary

This is a test for Falabella Retail based on microservices made in Spring Boot.

For this development, a set of tools, architectural standards and software design such as hexagonal architecture, clean Architecture, TDD, DDD and SOLID were used.
All this to guarantee scalability, maintenance, understanding of the code and easy deployment of it.

### Requirements

* [Maven](https://maven.apache.org/install.html)
* Java 8: Any compliant JVM should work.
  * [Java 8 JDK from Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html)


### Configuration

Capabilities are provided through dependencies in the pom.xml, ProductsConfiguration and the application.yml for the main properties.

### Project contents

The ports are set to the defaults of 8080 for http.

### Run

Opt 1:
To build and run the application using Maven:
sh
1. `mvn clean compile install`
2. `java -jar ./target/*.jar`

Opt 2:
To Open and run in a IDE:
To run the application you must open the project with an IDE, let the maven dependencies download and once downloaded, run the app with the ide tools, in the event that they are not downloaded alone, you can run `mvn install` in the root of the draft.


#### ENDPOINTS:

To create a product:
`POST: localhost:8080/products/create`

Example JSON: 
`{
   "sku": "FTest123",
   "name": "TestTest",
   "brand": "M",
   "size": "123",
   "price": "2000000",
   "principalImage": "https://img.com/phototest.jpg",
   "otherImages": ""
}`

To get all the products:
`GET: localhost:8080/products/findAll`

To search for a product by its SKU:
`GET localhost:8080/products/findBySku/TEST400`
