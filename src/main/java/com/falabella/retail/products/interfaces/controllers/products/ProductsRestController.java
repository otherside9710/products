package com.falabella.retail.products.interfaces.controllers.products;

import com.falabella.retail.products.application.exception.products.*;
import com.falabella.retail.products.application.use_cases.products.create.ProductsCreate;
import com.falabella.retail.products.application.use_cases.products.search.ProductsFindAll;
import com.falabella.retail.products.application.use_cases.products.search.ProductsFindBySKU;
import com.falabella.retail.products.domain.products.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/products/")
public class ProductsRestController implements IProductsRestController{

    @Autowired
    ProductsCreate productsCreate;

    @Autowired
    ProductsFindAll productsFindAll;

    @Autowired
    ProductsFindBySKU productsFindBySKU;

    @Override
    public ResponseEntity<?> save(@Valid @RequestBody Products products) {
        try {
            return new ResponseEntity<>(productsCreate.save(products), HttpStatus.CREATED);
        }catch (Products400Exception e) {
            return new ResponseEntity<>(Product400ErrorExceptionMessage.badRequest(e.getMessage()), HttpStatus.BAD_REQUEST);
        }catch (Exception e) {
            return new ResponseEntity<>(Product501ErrorExceptionMessage.internalError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> findAll() {
        try {
            return new ResponseEntity<>(productsFindAll.findAll(), HttpStatus.OK);
        }catch (Products404Exception e) {
            return new ResponseEntity<>(Product404ErrorExceptionMessage.notFound(e.getMessage()), HttpStatus.NOT_FOUND);
        }catch (Exception e) {
            return new ResponseEntity<>(Product501ErrorExceptionMessage.internalError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> findBySku(@PathVariable String sku) {
        try {
            return new ResponseEntity<>(productsFindBySKU.findBySku(sku), HttpStatus.OK);
        }catch (Products404Exception e) {
            return new ResponseEntity<>(Product400ErrorExceptionMessage.badRequest(e.getMessage()), HttpStatus.NOT_FOUND);
        }catch (Exception e) {
            return new ResponseEntity<>(Product501ErrorExceptionMessage.internalError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
