package com.falabella.retail.products.interfaces.controllers.products;

import com.falabella.retail.products.domain.products.Products;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public interface IProductsRestController {

    @PostMapping(value = "/create", produces = {"application/json"})
    ResponseEntity<?> save(@Valid @RequestBody Products products);

    @GetMapping(value = "/findAll", produces = {"application/json"})
    ResponseEntity<?> findAll();

    @GetMapping(value = "/findBySku/{sku}", produces = {"application/json"})
    ResponseEntity<?> findBySku(@PathVariable String sku);
}
