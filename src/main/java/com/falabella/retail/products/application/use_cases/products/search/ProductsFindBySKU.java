package com.falabella.retail.products.application.use_cases.products.search;

import com.falabella.retail.products.application.exception.products.Products404Exception;
import com.falabella.retail.products.application.exception.products.Products400Exception;
import com.falabella.retail.products.domain.products.Products;
import com.falabella.retail.products.domain.products.ProductsRepository;
import com.falabella.retail.products.infrastructure.repositories.products.ProductsRepositoryJPA;
import com.falabella.retail.products.infrastructure.orm.postgres.schema.ProductsEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class ProductsFindBySKU extends ProductsRepository {
    private final ProductsRepositoryJPA repository;

    public ProductsFindBySKU(ProductsRepositoryJPA repository) {
        this.repository = repository;
    }

    @Override
    public Products findBySku(String sku) throws Products400Exception {
        log.info("Fetching all products from db");
        Optional<ProductsEntity> entity = repository.findById(sku);

        if (!entity.isPresent()) {
            throw new Products404Exception("Product with SKU: " + sku + " is not found");
        }

        return entity.get().serialize();
    }
}
