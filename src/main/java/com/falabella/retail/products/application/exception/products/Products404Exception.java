package com.falabella.retail.products.application.exception.products;

public class Products404Exception extends RuntimeException{
    public Products404Exception(String message) {
        super(message);
    }
}
