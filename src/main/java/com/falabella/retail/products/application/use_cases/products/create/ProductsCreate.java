package com.falabella.retail.products.application.use_cases.products.create;

import com.falabella.retail.products.domain.products.Products;
import com.falabella.retail.products.domain.products.ProductsRepository;
import com.falabella.retail.products.application.exception.products.Products400Exception;
import com.falabella.retail.products.infrastructure.repositories.products.ProductsRepositoryJPA;
import com.falabella.retail.products.infrastructure.orm.postgres.schema.ProductsEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Slf4j
@Component
public class ProductsCreate extends ProductsRepository {
    private final ProductsRepositoryJPA repository;

    public ProductsCreate(ProductsRepositoryJPA repository) {
        this.repository = repository;
    }

    @Override
    public Products save(Products products) throws Products400Exception {
        log.info("Saving products dto");
        Optional<ProductsEntity> entity = repository.findById(products.getSku());

        if (entity.isPresent())
            throw new Products400Exception("Products Already exists, can't be saved");

        products.setCreateAt(new Date());

        return repository.save(ProductsEntity.createEntityFromDto(products)).serialize();
    }
}
