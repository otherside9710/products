package com.falabella.retail.products.application.exception.products;

import java.util.HashMap;
import java.util.Map;

public class Product404ErrorExceptionMessage {

    public static Map<String, Object> notFound(String message){
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", message);
        errorMap.put("code", 404);
        return errorMap;
    }
}
