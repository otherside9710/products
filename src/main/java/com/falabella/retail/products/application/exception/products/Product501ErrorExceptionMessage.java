package com.falabella.retail.products.application.exception.products;

import java.util.HashMap;
import java.util.Map;

public class Product501ErrorExceptionMessage {

    public static Map<String, Object> internalError(){
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", "Internal Server Error");
        errorMap.put("code", 501);
        return errorMap;
    }
}
