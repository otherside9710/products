package com.falabella.retail.products.application.exception.products;

public class Products400Exception extends RuntimeException{
    public Products400Exception(String message) {
        super(message);
    }
}
