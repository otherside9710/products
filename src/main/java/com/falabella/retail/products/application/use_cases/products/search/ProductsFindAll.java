package com.falabella.retail.products.application.use_cases.products.search;

import com.falabella.retail.products.application.exception.products.Products404Exception;
import com.falabella.retail.products.domain.products.Products;
import com.falabella.retail.products.domain.products.ProductsRepository;
import com.falabella.retail.products.infrastructure.repositories.products.ProductsRepositoryJPA;
import com.falabella.retail.products.infrastructure.orm.postgres.schema.ProductsEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ProductsFindAll extends ProductsRepository {
    private final ProductsRepositoryJPA repository;

    public ProductsFindAll(ProductsRepositoryJPA repository) {
        this.repository = repository;
    }

    @Override
    public List<Products> findAll() throws Products404Exception {
        log.info("Fetching all products from db");
        List<ProductsEntity> entities = repository.findAll();

        if (entities.isEmpty()) {
            throw new Products404Exception("Without data in the db");
        }
        log.info("{} rows fetched", entities.size());

        return entities.stream()
                .map(ProductsEntity::serialize)
                .collect(Collectors.toList());
    }
}
