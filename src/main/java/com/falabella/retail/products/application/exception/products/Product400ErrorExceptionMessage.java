package com.falabella.retail.products.application.exception.products;

import java.util.HashMap;
import java.util.Map;

public class Product400ErrorExceptionMessage {

    public static Map<String, Object> badRequest(String message){
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", message);
        errorMap.put("code", 400);
        return errorMap;
    }
}
