package com.falabella.retail.products.infrastructure.repositories.products;

import com.falabella.retail.products.infrastructure.orm.postgres.schema.ProductsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepositoryJPA extends JpaRepository<ProductsEntity, String> {

}
