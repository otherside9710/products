package com.falabella.retail.products.infrastructure.orm.postgres.schema;

import com.falabella.retail.products.domain.products.Products;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products")
public class ProductsEntity {
    @Id
    private String sku;
    private String name;
    private String brand;
    private String size;
    private Double price;
    @Column(name = "principal_image")
    private String principalImage;
    private String otherImages;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_at")
    private Date createAt;

    public Products serialize() {
        return new ModelMapper().map(this, Products.class);
    }

    public static ProductsEntity createEntityFromDto(Products products) {
        return new ModelMapper().map(products, ProductsEntity.class);
    }
}
