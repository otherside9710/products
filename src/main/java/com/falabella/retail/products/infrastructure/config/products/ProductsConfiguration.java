package com.falabella.retail.products.infrastructure.config.products;

import com.falabella.retail.products.application.use_cases.products.create.ProductsCreate;
import com.falabella.retail.products.infrastructure.repositories.products.ProductsRepositoryJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductsConfiguration {
    @Autowired
    ProductsRepositoryJPA repository;

    @Bean
    public ProductsCreate productsCreate() {
        return new ProductsCreate(repository);
    }
}
