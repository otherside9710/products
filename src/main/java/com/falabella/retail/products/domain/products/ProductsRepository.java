package com.falabella.retail.products.domain.products;

import com.falabella.retail.products.application.exception.products.Products404Exception;
import com.falabella.retail.products.application.exception.products.Products400Exception;

import java.util.List;
//TODO: Refactor to Interface \\ Abstract Class.
public class ProductsRepository{

    public Products save(Products products) throws Products400Exception {
        throw new Products400Exception("ERR_METHOD_NOT_IMPLEMENTED");
    }

    public List<Products> findAll() throws Products404Exception {
        throw new Products400Exception("ERR_METHOD_NOT_IMPLEMENTED");
    }

    public Products findBySku(String sku) throws Products404Exception {
        throw new Products400Exception("ERR_METHOD_NOT_IMPLEMENTED");
    }
}
