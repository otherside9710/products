package com.falabella.retail.products.domain.products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Date;

@NoArgsConstructor // Constructor Without Parameters
@AllArgsConstructor //Constructor With Parameters
@Data// Getter and Setter
public class Products {
    @NotNull
    @Size(min = 1000000, max = 99999999)
    private String sku;
    @NotNull
    @NotBlank
    @Size(min = 3, max = 50)
    private String name;
    @NotNull
    @NotBlank
    @Size(min = 3, max = 50)
    private String brand;
    @NotBlank
    private String size;
    @NotNull
    @Size(min = 1, max = 99999999)
    private Double price;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[A-Za-z0-9.-]+)((?:\\/[\\+~%\\/.\\w-_]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)$")
    private String principalImage;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[A-Za-z0-9.-]+)((?:\\/[\\+~%\\/.\\w-_]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)$")
    private String otherImages;
    private Date createAt;
}
