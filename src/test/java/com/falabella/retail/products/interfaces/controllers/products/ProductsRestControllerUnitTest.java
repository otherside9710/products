package com.falabella.retail.products.interfaces.controllers.products;

import com.falabella.retail.products.domain.products.Products;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@AutoConfigureMockMvc
public class ProductsRestControllerUnitTest {

    @Autowired
    ProductsRestController productsRestController;

    private String generateSku(){
        return "FAL" + (int) Math.floor(Math.random()*1000+1);
    }

    private Products getProduct(){
        Products product = new Products();
        product.setSku(this.generateSku());
        product.setName("Macbook Pro");
        product.setBrand("Apple");
        product.setSize("15 Inch");
        product.setPrice(10.000);
        product.setPrincipalImage("https://img.com/applemac.jpg");
        product.setOtherImages("");
        product.setCreateAt(new Date());

        return product;
    }

    @Test
    public void returnsOkWhenSavingANewProduct(){
        Products productData = this.getProduct();
        ResponseEntity<?> response = productsRestController.save(productData);
        Assert.assertSame("Test [returnsOkWhenSavingANewProduct]", HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void returns400WhenAProductAlreadyExists(){
        Products productData = this.getProduct();
        productData.setSku("TEST400");
        ResponseEntity<?> response = productsRestController.save(productData);
        Assert.assertSame("Test [returns400WhenAProductAlreadyExists]", HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void returnsAListOfProducts(){
        ResponseEntity<?> response = productsRestController.findAll();
        Assert.assertNotNull(response);
    }

    @Test
    public void returns404WhenThereAreNoProductsInBD(){
        ResponseEntity<?> response = productsRestController.findAll();
        Assert.assertSame("Test [returns404AWhenProductsNotExistInDB]",  HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void returnsAProductWhenASkuAlreadyExistInDB(){
        ResponseEntity<?> response = productsRestController.findBySku("TEST400");
        Assert.assertNotNull(response);
    }

    @Test
    public void returns404WhenThereIsNoProductInBD(){
        ResponseEntity<?> response = productsRestController.findBySku("NOTEXIST123");
        Assert.assertSame("Test [returns404AWhenProductsNotExistInDB]", HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}
